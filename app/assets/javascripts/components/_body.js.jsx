var Body = React.createClass({
  getInitialState() {
    return { accounts: [] }
  },

  componentDidMount() {
    $.getJSON('/api/v1/accounts.json', (response) => {
      this.setState({ accounts: response })
    });
  },

  handleSubmit(account) {
    var newState = this.state.accounts.concat(account);
    this.setState({ accounts: newState })
  },

  handleDelete(id) {
    $.ajax({
      url: `/api/v1/accounts/${id}`,
      type: 'DELETE',
      success: () => {
        this.removeAccountClient(id);
      }
    });
  },

  removeAccountClient(id) {
    var newAccounts = this.state.accounts.filter((account) => {
      return account.id != id;
    });

    this.setState({ accounts: newAccounts });
  },

  handleUpdate(account) {
    $.ajax({
      url: `/api/v1/accounts/${account.id}`,
      type: 'PUT',
      data: { account: account },
      success: () => {
        this.updateAccounts(account);
      }
    }
  )},

  updateAccounts(account) {
    var accounts = this.state.accounts.filter((i) => { return i.id != account.id });
    accounts.push(account);

    this.setState({ accounts: accounts });
  },

  render() {
    return (
      <div>
        <NewAccount handleSubmit={this.handleSubmit} />
        <AllAccounts accounts={this.state.accounts} handleDelete={this.handleDelete} onUpdate={this.handleUpdate} />
      </div>
    )
  }
});
