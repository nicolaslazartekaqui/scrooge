var Account = React.createClass({
  getInitialState() {
    return { editable: false }
  },

  handleEdit() {
    if(this.state.editable) {
      var id = this.props.account.id
      var name = this.refs.name.value;
      var account = { id: id, name: name }
      this.props.handleUpdate(account);
    }

    this.setState({ editable: !this.state.editable })
  },

  render() {
    var name = this.state.editable ? <input type='text' ref='name' defaultValue={this.props.account.name} /> : <h3>{this.props.account.name}</h3>;

    return (
      <div>
        {name}
        <button onClick={this.props.handleDelete}>Remover</button>
        <button onClick={this.handleEdit}> {this.state.editable ? 'Enviar' : 'Editar' } </button>
      </div>
    )
  }
});
