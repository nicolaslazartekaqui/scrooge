var AllAccounts = React.createClass({
  handleDelete(id) {
    this.props.handleDelete(id);
  },

  onUpdate(account) {
    this.props.onUpdate(account);
  },

  render() {
    var accounts = this.props.accounts.map((account) => {
      return (
        <div key={account.id}>
          <Account account={account}
            handleDelete={this.handleDelete.bind(this, account.id)}
            handleUpdate={this.onUpdate} />
        </div>
      )
    });

    return (
      <div>
        <h2>Accounts</h2>
        {accounts}
      </div>
    )
  }
});
