var NewAccount = React.createClass({
  handleClick() {
    var name = this.refs.name.value;
    $.ajax({
      url: '/api/v1/accounts',
      type: 'POST',
      data: { account: { name: name } },
      success: (account) => {
        this.props.handleSubmit(account);
      }
    });
  },

  render() {
    return (
      <div>
        <input ref="name" placeholder="Nome" />
        <button onClick={this.handleClick}>Enviar</button>
      </div>
    )
  }
});
