class CreateAccounts < ActiveRecord::Migration[5.0]
  def change
    create_table :accounts do |t|
      t.string :name
      t.float :opening_balance
      t.date :opening_balance_date

      t.timestamps
    end
  end
end
